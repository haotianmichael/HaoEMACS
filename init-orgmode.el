 
(require 'org)
(setq org-src-fontify-natively t)


;; 设置默认 Org Agenda 文件目录
(setq org-agenda-files '("~/Documents/Code"))
(global-set-key (kbd "C-c a") 'org-agenda);; 设置 org-agenda 打开快捷键

(provide 'init-orgmode)
