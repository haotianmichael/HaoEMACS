

(setq ring-bell-function 'ignore)

(tool-bar-mode -1)

(scroll-bar-mode -1)

(electric-indent-mode -1)

(global-linum-mode t)
(setq linum-format "%d")

(setq inhibit-splash-screen t)    ;;关掉启动画面

(global-auto-revert-mode t)

(setq-default cursor-type 'bar)

(setq make-backup-files nil)

(setq auto-save-default nil)

(delete-selection-mode t)

;;(setq initial-frame-alist (quote ((fullscreen . maximized))))



(global-hl-line-mode t)

(fset 'yes-or-no-p 'y-or-n-p)

(set-language-environment "UTF-8")

;;(global-set-key (kbd "C-w") 'backward-kill-word)
 


(provide 'init-betterDefaults)

